﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace TripManagement
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg)
        {
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string fileName = @"..\..\trips.txt";
        //public string savefileName = @"..\..\saved.trips";
        readonly List<Trip> _tripList = new List<Trip>();
        public MainWindow()
        {
            InitializeComponent();
            LoadFile();
        }
        private void LoadFile()
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    Trip trip = new Trip(line);
                    _tripList.Add(trip);
                    line = sr.ReadLine();
                }
                lvTrip.ItemsSource = _tripList;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            // Displays a SaveFileDialog so the user can save the file
            // assigned to Button2.
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "trips files (*.trips)|*.trips|All files (*.*)|*.*";
            saveFileDialog1.Title = "Save the selected the trip in a File";
            if (saveFileDialog1.ShowDialog() == true)
            {
                string allData = "";
                if (lvTrip.SelectedIndex == -1)
                {
                    MessageBox.Show("No trip is selected");
                }
                else
                {
                    foreach (Trip trip in lvTrip.SelectedItems)
                    {
                        allData += trip.ToString() + "\n";
                    }
                    File.WriteAllText(saveFileDialog1.FileName, allData);
                }
                
            }
            else
            {
                MessageBox.Show("File error", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnAddTrip_Click(object sender, RoutedEventArgs e)
        {
            string desination = "";
            if (TextDestination.Text != "")
            {
                if (TextDestination.Text.Contains(';'))
                {
                    MessageBox.Show("Destination must not contain semicolons");
                    return;
                }
                desination = TextDestination.Text;
            }
            else
            {
                throw new InvalidDataException("Destination must be filled");
            }
            
            string name = "";
            if (TextName.Text != "")
            {
                if (TextName.Text.Contains(';'))
                {
                    MessageBox.Show("Name must not contain semicolons");
                    return;
                }
                name = TextName.Text;
            }
            else
            {
                throw new InvalidDataException("Name must be filled");
            }

            string passport = "";
            if (TextPassport.Text != "")
            {
                passport = TextPassport.Text;
            }
            else
            {
                throw new InvalidDataException("Passport must be filled");
            }

            string departure = "";
            DateTime depTime;
            if (!DateTime.TryParse(DpDeparture.Text, out depTime))
            {
                throw new InvalidDataException("Departure must be filled");
            }
            else
            {
                departure = DpDeparture.DisplayDate.ToString();
            }

            string ret = "";
            DateTime retTime;
            if (!DateTime.TryParse(DpReturn.Text, out retTime))
            {
                throw new InvalidDataException("Departure must be filled");
            }
            else if (retTime <= depTime)
            {
                MessageBox.Show("Return date must be later than Departure date");
                return;
            }
            else
            {
                ret = DpReturn.DisplayDate.ToString();
            }

            Trip t = new Trip(desination,name, passport, departure, ret);
            _tripList.Add(t);

            ResetValue();
        }

        private void ResetValue()
        {
            lvTrip.Items.Refresh();
            TextDestination.Clear();
            TextName.Clear();
            TextPassport.Clear();
            DpDeparture.Text="";
            DpReturn.Text = "";
            lvTrip.SelectedIndex = -1;
        }

        private void BtnUpdateTrip_Click(object sender, RoutedEventArgs e)
        {
            if (lvTrip.SelectedIndex == -1)
            {
                MessageBox.Show("you need to choose one trip");
                return;
            }

            string desination = "";
            if (TextDestination.Text != "")
            {
                if (TextDestination.Text.Contains(';'))
                {
                    MessageBox.Show("Destination must not contain semicolons");
                    return;
                }
                desination = TextDestination.Text;
            }
            else
            {
                throw new InvalidDataException("Destination must be filled");
            }

            string name = "";
            if (TextName.Text != "")
            {
                if (TextName.Text.Contains(';'))
                {
                    MessageBox.Show("Name must not contain semicolons");
                    return;
                }
                name = TextName.Text;
            }
            else
            {
                throw new InvalidDataException("Name must be filled");
            }

            string passport = "";
            if (TextPassport.Text != "")
            {
                passport = TextPassport.Text;
            }
            else
            {
                throw new InvalidDataException("Passport must be filled");
            }

            string departure = "";
            DateTime depTime;
            if (!DateTime.TryParse(DpDeparture.Text, out depTime))
            {
                throw new InvalidDataException("Departure must be filled");
            }
            else
            {
                departure = DpDeparture.DisplayDate.ToString();
            }

            string ret = "";
            DateTime retTime;
            if (!DateTime.TryParse(DpReturn.Text, out retTime))
            {
                throw new InvalidDataException("Departure must be filled");
            }
            else if (retTime <= depTime)
            {
                MessageBox.Show("Return date must be later than Departure date");
                return;
            }
            else
            {
                ret = DpReturn.DisplayDate.ToString();
            }

            Trip tripTobeUpdated = (Trip)lvTrip.SelectedItem;
            tripTobeUpdated.Destination = desination;
            tripTobeUpdated.Name = name;
            tripTobeUpdated.Passport = passport;
            tripTobeUpdated.DepartureDate = departure;
            tripTobeUpdated.ReturnDate = ret;

            ResetValue();
        }

        private void LvTrip_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BtnDeleteTrip.IsEnabled = true;
            BtnUpdateTrip.IsEnabled = true;

            var selectedTrip = lvTrip.SelectedItem;

            if (selectedTrip is Trip)
            {
                Trip trip = (Trip)lvTrip.SelectedItem;
                TextDestination.Text = trip.Destination;
                TextName.Text = trip.Name;
                TextPassport.Text = trip.Passport;
                DpDeparture.Text = trip.DepartureDate.ToString();
                DpReturn.Text = trip.ReturnDate.ToString();
            }
        }

        private void BtnDeleteTrip_Click(object sender, RoutedEventArgs e)
        {
            if (lvTrip.SelectedIndex == -1)
            {
                MessageBox.Show("you need to choose one Trip");
                return;
            }

            MessageBoxResult result = MessageBox.Show("Are you sure to delete?", "CONFIRMATION", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                Trip tripTobeDeleted = (Trip) lvTrip.SelectedItem;
                _tripList.Remove(tripTobeDeleted);

                ResetValue();
            }
        }
    }
}
