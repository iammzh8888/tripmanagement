﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace TripManagement
{
    class Trip
    {
        public delegate void LoggerDelegate(string reason);

        public static LoggerDelegate LogFailSet;


        private string _destination;
        public string Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                _destination = value; ;
            }
        }

        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value; ;
            }
        }

        public string Passport { get; set; }

        private string _departureDate;
        public string DepartureDate
        {
            get
            {
                return _departureDate;
            }
            set
            {
                if (!DateTime.TryParse(value, out DateTime dep))
                {
                    LogFailSet?.Invoke("Departure must be a Date");
                    throw new InvalidDataException("Departure must be a Date:\n" + value);
                }
                this._departureDate = value;
            }
        }

        private string _returnDate;
        public string ReturnDate {
            get
            {
                return _returnDate;
            }
            set
            {
                if (!DateTime.TryParse(value, out DateTime ret))
                {
                    LogFailSet?.Invoke("Return must be a Date");
                    throw new InvalidDataException("Return must be a Date:\n" + value);
                }
                this._returnDate = value;
            }
        }

        public Trip(string destination, string name, string passport, string departureDate, string returnDate)
        {
            this.Destination = destination;
            this.Name = name;
            this.Passport = passport;
            this.DepartureDate = departureDate;
            this.ReturnDate = returnDate;
        }

        public Trip(string dataLine)
        {
            string[] tripStrings = dataLine.Split(';');
            if (tripStrings.Length != 5)
            {
                LogFailSet?.Invoke("Line has invalid number");
                throw new InvalidDataException("Line has invalid number for fields:\n" + dataLine);
            }
            this.Destination = tripStrings[0];
            this.Name = tripStrings[1];
            this.Passport = tripStrings[2];
            DateTime dep;
            if (!DateTime.TryParse(tripStrings[3], out dep))
            {
                LogFailSet?.Invoke("Departure must be a Date");
                throw new InvalidDataException("Departure must be a Date:\n" + dataLine);
            }
            this.DepartureDate = tripStrings[3];
            DateTime ret;
            if (!DateTime.TryParse(tripStrings[4], out ret))
            {
                LogFailSet?.Invoke("Return must be a Date");
                throw new InvalidDataException("Return must be a Date:\n" + dataLine);
            }else if (ret<=dep)
            {
                MessageBox.Show("Return date must be later than Departure date");
                return;
            }
            this.ReturnDate = tripStrings[4];
        }

        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};{4}", this.Destination, this.Name, this.Passport, this.DepartureDate,
                this.ReturnDate);
        }
    }
}
